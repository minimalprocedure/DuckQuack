import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import javax.script.ScriptException

engine_mng = ScriptEngineManager.new
js_engine = engine_mng.getEngineByName("nashorn") 

js_engine.eval("function sum(x, y) {return x + y}")

puts js_engine.eval("sum(4,2)")

