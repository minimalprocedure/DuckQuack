ritmo = ritmo_musicale([
  "O..oO...O..oOO..",
  "..S...S...S...S.",
  "````````````````",
  "...............+"
], repeat: 1, misure: 10)

esecutore_musicale(
  :pattern => ritmo
)