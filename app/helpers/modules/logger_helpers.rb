# encoding: utf-8
################################################################################
## Initial developer: Massimo Maria Ghisalberti <massimo.ghisalberti@gmail.org>
## Date: 2018-12-18
## Company: Pragmas <contact.info@pragmas.org>
## Licence: Apache License Version 2.0, http://www.apache.org/licenses/
################################################################################

module LoggerHelpers

  class Log
    class << self

      def caller_line_time
        l = caller_locations(2, 1).first.tap{ |l| l }
        preamble_size = app.main_controller.source_controller.preamble.split("\n").size
        "[#{l.path}|#{l.lineno - preamble_size}|#{l.label}|#{Time.now}]"
      end

      def info(m)
        puts "I: #{caller_line_time}: #{m}"
      end

      def debug(m)
        puts "D: #{caller_line_time}: #{m}"
      end

      def warn(m)
        puts "W: #{caller_line_time}: #{m}"
      end
    end

  end

end