# encoding: utf-8
################################################################################
## Initial developer: Massimo Maria Ghisalberti <massimo.ghisalberti@gmail.org>
## Date: 2016-12-18
## Company: Pragmas <contact.info@pragmas.org>
## Licence: Apache License Version 2.0, http://www.apache.org/licenses/
################################################################################

include Java
import java.lang.System

require 'jrubyfx'
require 'sinatra/base'
require 'json'
require 'faraday'

module HttpHelpers

  def http_get(opts = {})
    params = {
      :url => ''
    }.deep_merge(opts)
    url = params.delete(:url)
    conn = Faraday.new(url, params)
    conn.get
  end

  class HttpServer < Sinatra::Base

    def initialize(app = nil)
      super()
      puts '============ HttpServer ======================='
      puts 'use /kill route for stopping server'
      puts '==============================================='
    end

    set :port, app.configs.fetch2([:web_server_port], 3000)
    set :host, '127.0.0.1'
    set :bind, '0.0.0.0'

    class << self
      def stop_httpd
        Process.kill('TERM', Process.pid)
      end
    end

    get '/kill' do
      if request.host == '127.0.0.1' ||
         request.host == '0.0.0.0'   ||
         request.host == 'localhost'
        HttpServer.stop_httpd
        'bye!'
      else
        'You can\'t stop server!'
      end
    end

  end


end
