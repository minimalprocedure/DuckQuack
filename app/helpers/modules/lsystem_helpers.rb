# encoding: utf-8
################################################################################
## Initial developer: Massimo Maria Ghisalberti <massimo.ghisalberti@gmail.org>
## Date: 2018-12-18
## Company: Pragmas <contact.info@pragmas.org>
## Licence: Apache License Version 2.0, http://www.apache.org/licenses/
################################################################################

module LsystemHelpers

  class LSystem

    attr :system

    MAX_GENERATIONS = 3

    def initialize(axiom, rules, constants, generations)
      @axiom, @rules, @constants, @generations = axiom, rules, constants, generations
      @action_calls = {}
      @system = evolve_with_rules
    end

    def is_digit(c)
      c == '0' ? true : !c.to_s.to_i.zero?
    end
    private :is_digit

    def evolve_with_rules
      apply_rules = lambda { |s, gen|
        @rules.reduce(s) { |sys, r|
          @constants.include?(r[0]) ? sys : sys.gsub(r[0].to_s, r[1])
        }.gsub('|', gen.to_s)
      }
      logger.info("L-System max generations is #{MAX_GENERATIONS}") if MAX_GENERATIONS < @generations
      (1...@generations).to_a[0, MAX_GENERATIONS].reduce(@axiom) { |sys, i| apply_rules.call(sys, i) } 
    end
    private :evolve_with_rules

    def execute_default_action(actions, g, i)
      actions[:'_'].call(g, i) unless actions[:'_'].nil?
    end
    private :execute_default_action

    def update_grow_iters(action, generation)
      if is_digit(action)
        if @action_calls.include?(action)
          @action_calls[action] += 1
          [generation, @action_calls[action]]
        else
          @action_calls[action] = 0
          [0, 0]
        end
      else
        [0, 0]
      end
    end

    def execute(actions)
      generation = '0'
      chars = @system.split(//)

      default_generation_action = ->(i) {
        if(generation.size != 1)
          execute_default_action(actions, generation.to_i, i)
          generation = '0'
        end
      }

      chars.each_index { |i|
        action = chars[i].to_sym
        generation << action.to_s if is_digit(action)
        (g, i) = update_grow_iters(action, generation.to_i)
        if !is_digit(action) && actions.include?(action)
          actions[action].call(g, i)
        else
          default_generation_action.call(i)
        end
      }
    end
  end

end