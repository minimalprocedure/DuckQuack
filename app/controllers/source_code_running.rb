# encoding: utf-8
################################################################################
## Initial developer: Massimo Maria Ghisalberti <massimo.ghisalberti@gmail.org>
## Date: 2016-12-18
## Company: Pragmas <contact.info@pragmas.org>
## Licence: Apache License Version 2.0, http://www.apache.org/licenses/
################################################################################

include Java
import java.lang.System

require 'jrubyfx'
import javafx.application.Platform
require 'yaml'
require 'fileutils'
require 'facets'

class RunningCode

  attr_reader :canvas
  attr_reader :container
  attr_reader :graphic_context
  attr_reader :output
  attr_reader :source_controller

  attr_accessor :__http_process_pid

  require 'code_running_helpers'
  include CodeRunningHelpers

  path = File.join(app.configs.fetch2([:path, :locale], './'), app.configs.fetch2([:lang], 'en'))
  $LOAD_PATH << path unless $LOAD_PATH.include?(path)
  Dir[File.join(path, '*.rb')].each { |m|
    require(m)
    name = File.basename(m).slice(0..-4)
    include const_get(name.modulize)
  }

  def initialize(container, source_controller, canvas, output)
    @__http_process_pid = nil
    set_async_or_not
    @output = output
    @source_controller = source_controller
    @container = container
    @canvas = canvas
    inject_gc_methods
    inject_canvas_methods
    generate_methods_list if app.configs.fetch2([:generate_methods_list], false)
    inject_methods_alias
  end  

  def set_async_or_not
    @async_type = app.configs.fetch2([:code_runner, :type], :task)
    @async_runner = app.configs.fetch2([:code_runner, :async], false)
    @async_type = :sync if !@async_runner
  end
  private :set_async_or_not

  def inject_canvas_methods
    @canvas.methods.sort.each { |m|
      unless respond_to?(m)
        if @canvas.method(m).arity == 0
          self.class.send(:define_method, m) { @canvas.send(m) }
        else
          self.class.send(:define_method, m) { |*args| @canvas.send(m, *args) }
        end
      end
    }
  end
  private :inject_canvas_methods

  def inject_gc_methods
    @graphic_context = @canvas.getGraphicsContext2D
    @graphic_context.methods.each { |m|
      unless respond_to?(m)
        if @graphic_context.method(m).arity == 0
          self.class.send(:define_method, m) { @graphic_context.send(m) }
        else
          self.class.send(:define_method, m) { |*args| @graphic_context.send(m, *args) }
        end
      end
    }
  end
  private :inject_gc_methods

  def generate_methods_list
    path = File.join(app.configs.fetch2([:path, :locale], './'), 'en')
    FileUtils.mkdir_p(path)
    locale = File.join(app.configs.fetch2([:path, :locale], './'), 'en', 'locale.yml')
    loc_hash = File.exist?(locale) ? YAML.load_file(locale).deep_rekey { |k| k.to_sym } :
      {:key => 'value', :methods => [{ :name => '', :alias => '' }]}
    h = methods.sort.reduce({}) { |acc, m| acc[m.to_s] = m.to_s; acc }
    File.open(locale, 'w') { |f|
      loc_hash[:en][:methods] = h
      loc_hash.deep_rekey! { |k| k.to_s }
      f.write(loc_hash.to_yaml)
    }
  end
  private :generate_methods_list

  def inject_methods_alias
    methods.sort.each { |m|
      a = app.t_method(m).to_sym
      if (a.to_s != m.to_s) #&& (m != :activate)
        logger.debug("ALIAS: #{a} for #{m}")
        self.class.send(:alias_method, a, m) if !respond_to?(a)
      end
    }
  end
  private :inject_methods_alias

  def evaluate_source(language, source, filename, starting_line)
    case language.to_s.upcase
    when "RUBY"
      instance_eval(source, filename,
                    starting_line <= 0 ? 1 : starting_line)
    else
      language = "nashorn" if language == "JAVASCRIPT"
      import javax.script.Bindings
      import javax.script.ScriptEngine
      import javax.script.ScriptEngineManager
      import javax.script.ScriptException
      import javax.script.SimpleBindings
      import javax.script.ScriptContext
      import javax.script.SimpleScriptContext
      engine_mng = ScriptEngineManager.new
      engine = engine_mng.getEngineByName(language)
      ctx = SimpleScriptContext.new
      #ctx.setAttribute("Turtle", Turtle.become_java!, ScriptContext.ENGINE_SCOPE); 
      engine.eval(source, ctx)
    end
  end

  def activate_sync
    s = @source_controller.code_get
    preamble_size = s.fetch2([:preamble_size], 0) + 1
    language = s.fetch2([:language], app.configs[:language]) 
    executed_filename = s.fetch2([:filename], "FILENAME") #">>>> #{app.t(:executed_filename)} >>>> "
    code = s.fetch2([:code], '')
    begin     
      evaluate_source(language, code, executed_filename, -preamble_size)
    rescue Exception => excp
      backtrace = excp.respond_to?(:backtrace_locations) ? excp.backtrace_locations.join("\n") : excp.backtrace.join("\n")
      message = %(MESSAGE:\n#{excp.message}\nBACKTRACE:\n#{backtrace})
      linenumber,
      cause =
      if language.to_s.upcase == "RUBY"
        m = message.match(Regexp.new(Regexp.escape(executed_filename).to_s + ':(?<linenumber>\d*)(?<cause>.*|:in)'))
        [m['linenumber'].to_i - preamble_size, m['cause']]
      else
        m = message.match(Regexp.new('<eval>:(?<linenumber>\d+):(?<char>\d+)(?<cause>.*)'))
        unless m.nil?          
          [m['linenumber'].to_i - preamble_size, m['cause']]
        else
          m = message.match(Regexp.new('at line number (?<linenumber>\d+)'))
          unless m.nil?
            [m['linenumber'].to_i - preamble_size, excp.message]
          else
            [0, excp.message]
          end
        end
      end
      @source_controller.syntax_highlighter.set_error_point(linenumber, cause)
      println("\n#{message}")
    end
  end

  class TaskActivate < Java::javafx.concurrent.Task
    attr_accessor :runner
    def call
      Platform.runLater(-> { @runner.activate_sync })
    end
  end

  def activate_async_task
    task = TaskActivate.new
    task.runner = self
    ExecutorsPool['RunningCode'].execute(task)
  end

  def activate_async_later(runner = self)
    Platform.runLater(-> { runner.activate_sync })
  end

  def activate
    @async_type = :later
    logger.debug("CODE RUNNING MODE: #{ @async_type.to_s.upcase }")    
    case @async_type 
    when :task      
      activate_async_task
    when :later
      activate_async_later
    when :sync
      activate_sync
    end
  end

  def clean_gc
    System.gc
  end

end
